#!/bin/sh

echo "Running tests"

NWIN=$(cat tests.scm | ./myy | grep "(OK " | wc -l)
NTESTS=$(cat tests.scm | grep "^(check" | wc -l)

test "$NWIN" -eq "$NTESTS" && echo "All OK. You can now run ./myy" && exit 0

cat tests.scm | ./myy

echo "Failure: $NWIN / $NTESTS tests pass"

exit 1

