#!/bin/sh

DEV=sda1
FILE=$1

echo "Ensuring root privileges"
whoami | grep -q root || exit 1

echo "Waiting for Pico at /dev/$DEV"

while true
do
   dmesg | tail -n 2 | grep "$DEV" && break 
   echo -n "o"
   sleep 0.5
done

echo "Mounting /dev/$DEV to /mnt"
sudo mount "/dev/$DEV" /mnt || exit 1

cp -v "$FILE" /mnt
echo "Unmounting /mnt for good measure"
umount /mnt 
echo "Installed"

