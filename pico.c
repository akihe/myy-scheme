// Code specific to running on RP2040

#include <pico/stdlib.h>
#include <hardware/flash.h>
#include <hardware/sync.h>
#include <hardware/dma.h>
#include <hardware/adc.h>
#include <hardware/gpio.h>
#include <hardware/i2c.h>
#include <pico/unique_id.h>
#include <tusb.h>
#include <stdlib.h>

#define DATA 1024
uint16_t databuff[DATA]; // a buffer for raw data (ADC)

const uint LED_PIN = PICO_DEFAULT_LED_PIN;

uint16_t sys(int op, int arg, int brg) {
   // printf("vm: system op %d, %d, %d\n", op, arg, brg);
   switch(op) {
      case 0: // 0, = gpio init, move to init?
         gpio_init(arg);
         return 1;
      case 1: //
         gpio_put(arg, brg);
         return 1;
      case 5:  // usleep
         sleep_us(arg);
         return 1;
      case 6: // select ADC input, 0-3 = pins 26 - 29, 4 = onboard temperature
         adc_select_input(arg);
         return 1;
      case 7: // read ADC, one sample
         return adc_read();
      case 8:  // set adc sampling speed
         adc_set_clkdiv(48000000 / arg);
         printf("++ set clock div %d\n", 48000000 / arg);
         return 1;
      case 9: { // fill adc buffer
         int p;
         uint dma_chan = dma_claim_unused_channel(true);
         printf("selected dma channel %d\n", dma_chan);
         //dma_channel_set_read_addr(&adc_hw->fifo);
         //dma_channel_set_write_Addr(databuff);

         dma_channel_config cfg = dma_channel_get_default_config(dma_chan);
         adc_fifo_drain(); // clear and stop
         adc_run(false);
         adc_fifo_setup(
            true,    // Write each completed conversion to the sample FIFO
            true,    // Enable DMA data request (DREQ)
            1,       // DREQ (and IRQ) asserted when at least 1 sample present
            true,    // keep sample error bit (bit 12, check for it!)
            false    // keep full 12 bits, middle
         );

         // Reading from constant address, writing to incrementing byte addresses
         channel_config_set_transfer_data_size(&cfg, DMA_SIZE_16);
         channel_config_set_dreq(&cfg, DREQ_ADC); // adc controls speed
         channel_config_set_read_increment(&cfg, false); // adc result at fixed place
         channel_config_set_write_increment(&cfg, true); // move output pointer
         for (p = 0; p < DATA; p++)
            databuff[p] = 0;

         dma_channel_configure(dma_chan, &cfg,
            &databuff,
            &adc_hw->fifo,
            DATA, 1);
         adc_run(true);
         printf("++ adc runningo\n");
         dma_channel_wait_for_finish_blocking(dma_chan);
         printf("++ adc done\n");
         adc_run(false);
         adc_fifo_drain();
         //for (p = 0; p < 100; p++)
         //   printf("%d ", databuff[p]);
         dma_channel_unclaim(dma_chan);
         return 1;
      }
      case 10: {
         i2c_init(i2c_default, 100 * 1000);
         // initialize default i2c pins 4 & 5 (though we have options)
         gpio_set_function(PICO_DEFAULT_I2C_SDA_PIN, GPIO_FUNC_I2C);
         gpio_set_function(PICO_DEFAULT_I2C_SCL_PIN, GPIO_FUNC_I2C);
         gpio_pull_up(PICO_DEFAULT_I2C_SDA_PIN);
         gpio_pull_up(PICO_DEFAULT_I2C_SCL_PIN);
         // bi_decl(bi_2pins_with_func(PICO_DEFAULT_I2C_SDA_PIN, PICO_DEFAULT_I2C_SCL_PIN, GPIO_FUNC_I2C));
         return 1;
      }
      case 11: { // i2c read byte (to scan)
         int ret;
         uint8_t rxdata;
         ret = i2c_read_blocking(i2c0, arg, &rxdata, 1, false);
         if (ret < 0 || ret == PICO_ERROR_GENERIC) {
            return 99999;
         }
         return rxdata;
      }
      case 12: { // gpio get, pin should be in input mode (gpio 2 pin)
         return gpio_get(arg);
      }
      case 13: { // get pico unique id, temporarily just low bits
         pico_unique_board_id_t board_id;
         pico_get_unique_board_id(&board_id);
         if (0 <= arg && arg < PICO_UNIQUE_BOARD_ID_SIZE_BYTES) {
         }
         return fixnum(board_id.id[0] + (board_id.id[1] << 8) + (board_id.id[2] << 16));
      }
      case 14: { // switch to 64-bit time when bignums are available
         uint32_t t = time_us_32() / 10000;
         return fixnum(t % 0xffffff);
      }
      case 15:  // sleep milliseconds
         sleep_ms(arg);
         return 1;
      case 16:  // system info (0 = RP2040, 1 = UNIX)
         return 0;
      case 17:  // adc gpio init
         adc_gpio_init(arg);
         return 1;
      case 18: // pin output?
         gpio_set_dir(arg, (brg ? GPIO_OUT : GPIO_IN));
        return 1;
      default:
         return 0;
   }
}

char emit(int n) {
   printf("%c", n & 255);
   //sleep_ms(100);
   return 1;
}

// everything is no
void panic_blink() {
   int pin = 25;
   int n = 1;
   gpio_init(pin);
   gpio_set_dir(pin, GPIO_OUT);
   while(1) {
      gpio_put(pin, n);
      sleep_us(300000);
      n = !n;
   }
}

void myy_exit(int n) {
   panic_blink();
}

char get_char() {
   char c;
   int light = 1;
   int dir = 1;
   while((c = getchar_timeout_us(0)) == 255) {
      sleep_us(1000);
   }
   return c;
}

char memory_mismatch(uint8_t *f, uint8_t *t, size_t len) {
   int *from = (int *) f;
   int *to = (int *) t;
   len >>= 2;
   // measure in words, mainly to catch unlikely alignment and endianess issues
   while(len--) {
      if (*from++ != *to++)
         return 1;
   }
   return 0;
}


// start writing at 512kb after heap

#define FLASH_HEAP_OFFSET (512 * 1024)
#define FLASH_HEAP_STEP   (256 * 1024)
#define MYY_VERSION 1

char write_flash(int flash_offset, char *data, size_t len, int ver) {
   char *start = data;
   char mark[FLASH_PAGE_SIZE];
   uint32_t ints;
   int pos = 0;
   while(pos < FLASH_PAGE_SIZE) {
      mark[pos] = (pos + ver) & 63;
      pos++;
   }
   ints = save_and_disable_interrupts();
   // clear target flash range
   flash_range_erase(flash_offset,
      ((len / FLASH_SECTOR_SIZE) + 1) * FLASH_SECTOR_SIZE);
   flash_range_program(flash_offset, data, len);
   restore_interrupts(ints);
   if (memory_mismatch(start, (uint8_t *) (XIP_BASE + FLASH_HEAP_OFFSET), len)) {
      printf(";; MEMORY WRITE FAILED\n");
      return 0;
   }
   ints = save_and_disable_interrupts();
   flash_range_erase(flash_offset - FLASH_SECTOR_SIZE, FLASH_SECTOR_SIZE);
   flash_range_program(flash_offset - FLASH_SECTOR_SIZE, mark, FLASH_PAGE_SIZE);
   restore_interrupts(ints);
   if (memory_mismatch(start, (uint8_t *) (XIP_BASE + FLASH_HEAP_OFFSET), len)) {
      printf(";; MEMORY WRITE FAILED 2\n");
      return 0;
   }
   printf("Saved and marked\n");
   return 1;
}

void init () {
   stdio_init_all();
   adc_init(); // initialize adc conversion
   // while (!tud_cdc_connected()) {} // don't wait for usb
}

char write_heap(char *start, int cells, int ver) {
   return write_flash(FLASH_HEAP_OFFSET, start, cells*4*2, ver);
}

char load_heap(char *start, int cells, int ver) {
   uint8_t *pos = start;
   uint8_t *flash = (uint8_t *) (XIP_BASE + FLASH_HEAP_OFFSET);
   int p = 0;
   //printf("vm: loading heap for version %d\n", ver);
   while(p < FLASH_PAGE_SIZE) {
      if (flash[p - FLASH_SECTOR_SIZE] != ((p + ver) & 63)) {
         printf("vm: restore magic wrong at %d for %d\n", p, ver);
         return 0;
      }
      p++;
   }
   //printf("vm: magic ok for version %d\n", ver);
   memcpy(start, flash, cells*4*2);
   return 1;
}

const int *flash_heap_root = (const int *) ((int)XIP_BASE + (int)FLASH_HEAP_OFFSET);

