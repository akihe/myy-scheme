;;;
;;; Settings
;;;

;; settings -object is used to changed behavior of the repl
;; settings = (exit-on-errors save-sources? builtin-heap-version)

(define (exit-on-errors!)
   (set-car! *settings* 1))

(define (resume-on-errors!)
   (set-car! *settings* 0))

(define (save-sources!)
   (set-car! (cdr *settings*) #t))

(define (drop-sources!)
   (set-car! (cdr *settings*) #f))

(define (update-version!)
   (set-car! (cddr *settings*) (band 65535 (+ 1 (car (cddr *settings*))))))

(define *version* "Myy Scheme 0.1c")

(exit-on-errors!)
(drop-sources!)
(update-version!)


;;;
;;; Primitives
;;;

;; Calls to primitives currently override other bindings, so one
;; can wrap them into a functions like this

(define (+ a b) (+ a b))
(define (* a b) (* a b))
(define (/ a b) (/ a b))
(define (- a b) (- a b))
(define (cons a b) (cons a b))
(define (emit a)   (emit a))
(define (car a) (car a))
(define (cdr a) (cdr a))
;;;
;;; Simple Derived Operations
;;;

(define (% a b) (- a (* b (/ a b) )))
(define (= a b) (eq? a b))
(define (> a b) (or (< b a) (eq? a b)))

(define (null? x) (eq? x '()))

(define (boolean? x)
   (or (eq? x #t) (eq? x #f)))

(define (caar x) (car (car x)))
(define (cadr x) (car (cdr x)))
(define (cdar x) (cdr (car x)))
(define (cddr x) (cdr (cdr x)))

(define (caaar x) (car (car (car x)))))
(define (caadr x) (car (car (cdr x)))))
(define (cadar x) (car (cdr (car x)))))
(define (caddr x) (car (cdr (cdr x)))))
(define (cdaar x) (cdr (car (car x)))))
(define (cdadr x) (cdr (car (cdr x)))))
(define (cddar x) (cdr (cdr (car x)))))
(define (cdddr x) (cdr (cdr (cdr x)))))

(define (not x) (if x #f #t))
(define (null? x) (eq? x '()))
(define (zero? x) (eq? x '()))

(define list (lambda x x))

;;; Types

;; 4 = 100 = allocated + pair/fixnum bit
(define (pair? x) (eq? 4 (pig x)))

; 6 = 110 = immediate + pair/fixnum bit
(define (number? x) (eq? 6 (pig x)))

;; Functions are allocated and not pairs. They are values where the first
;; element is a non-empty list. This way the code of the function also acts
;; as type information.

(define (function? x)
   (if (eq? 0 (pig x))
      (pair? (car (set-pig x 4)))
      #f))

(define (symbol? x)
   (if (eq? 0 (pig x))
      (eq? '() (car (set-pig x 4)))
      #f))

;; bignum = pair of boolean (positive?) and list of digits?


;;;
;;; List Functions
;;;

; immediate/list equal?

(define (fold o s l)
   (if (eq? l '()) s (fold o (o s (car l)) (cdr l))))

(define (foldr o s l)
   (if (eq? l '()) s (o (car l) (foldr o s (cdr l)))))

(define (equal? a b)
   (or
      (eq? a b)
      (and (pair? a)
           (pair? b)
           (equal? (car a) (car b))
           (equal? (cdr a) (cdr b)))))

(define (append a b)
   (foldr (lambda (x t) (cons x t)) b a))

(define (lref lst n)
   (if (eq? n 0)
      (car lst)
      (lref (cdr lst) (- n 1))))

(define (reverse l)
   (fold (lambda (a b) (cons b a)) '() l))

(define (map f l)
   (foldr (lambda (a b) (cons (f a) b)) '() l))

(define (length l)
   (fold (lambda (a b) (+ a 1)) 0 l))

(define (iota f s e)
    (if (eq? f e) '() (cons f (iota (+ f s) s e))))

(define (foldn o s l)
   (if (eq? l '())
      s
      (let* ((s (o s l)))
         ;; make order explicit
         (foldn o s (cdr l)))))

(define (for-each op l)
   (fold (lambda (s x) (op x)) l l))

(define (list? x)
   (if (eq? x '())
      #t
      (if (pair? x)
         (list? (cdr x))
         #f)))

;; possibly improper -> proper list
(define (enlist x)
   (cond
      ((null? x) x)
      ((pair? x)
         (cons (car x) (enlist (cdr x))))
      (else
         (list x))))

(define (getn l k d)
   (cond
      ((eq? l '()) d)
      ((eq? (caar l) k)
         (car l))
      (else
         (getn (cdr l) k d))))

(define (getq l k d)
   (cdr (getn l k (cons #f d))))

(define (has? lst x)
   (cond
      ((eq? lst '()) #f)
      ((eq? (car lst) x) #t)
      (else (has? (cdr lst) x))))

(define (zip op la lb)
   (cond
      ((eq? la '()) la)
      ((eq? lb '()) lb)
      (else
         (cons (op (car la) (car lb))
            (zip op (cdr la) (cdr lb))))))


;;;
;;; Strings
;;;

;; pair of #t and list of characters

(define (list->string chars)
   (set-pig (cons #t chars) 0))

(define (string? x)
   (if (eq? 0 (pig x))
      (eq? #t (car (set-pig x 4)))
      #f))

(define (string->list str)
   (if (string? str)
      (cdr (set-pig str 4))
      '()))

(define (string-equal? a b)
   (equal?
      (set-pig a 4)
      (set-pig b 4)))


;;;
;;; Symbols
;;;

;; pair of null and string

(define (symbol->string symbol)
   (if (symbol? symbol)
      (cdr (set-pig symbol 4))
      #f))

(define (new-symbol s)
   (set-pig (cons '() s) 0))

(define (gensym)
   ;; not interned, so always unique as in eq?
   (new-symbol "G"))

;; note: it is important to use the same *symbols* object across
;; generations. the parser and compiler depend on it.

(define (intern! s)
   (foldn
      (lambda (found x)
         (cond
            (found found)
            ((string-equal? s (symbol->string (car x)))
               (car x))
            ((null? (cdr x))
               (set-cdr! x (list (new-symbol s)))
               #f)
            (else #f)))
      #f *symbols*))


;;;
;;; Rendering Values to Text
;;;

(define (negate x) (- 0 x))

(define (remainders x)
   (if (eq? x 0)
      '()
      (let* ((q (/ x 10)) (r (- x (* q 10))))
         (cons r (remainders q)))))

(define (render-number x tl)
   (cond
      ((eq? x 0)
         (cons 48 tl))
      ((< x 0)
         (cons 45
            (render-number (negate x) tl)))
      (else
         (append
            (map
               (lambda (x) (+ x 48))
               (reverse (remainders x)))
            tl))))

(define (render-list render l tl)
   (foldr
      (lambda (a b)
         (render a
            (if (eq? b tl)
               b
               (cons 32 b))))
      tl l))

(define (render x tl)
   (cond
      ((string? x)
         (append (string->list x) tl))
      ((symbol? x)
         (append (string->list (symbol->string x)) tl))
      ((eq? x #t)
         (cons 35 (cons 116 tl)))
      ((eq? x #f)
         (cons 35 (cons 102 tl)))
      ((number? x)
         (render-number x tl))
      ((list? x)
         (cons 40
            (render-list render x
               (cons 41 tl))))
      ((pair? x)
         (cons 40
            (render (car x)
               (cons 32
                  (cons 46
                     (cons 32
                        (render (cdr x)
                           (cons 41 tl))))))))
      ((function? x)
         (append
            '(35 60 102 117 110 99 116 105 111 110 62)
            tl))
      (else
         (cons 63 tl))))

(define (display-chars cs)
   (for-each emit cs))

(define (display x)
   (display-chars
      (render x '())))

(define (print . args)
   (for-each display args)
   (emit 10))


;;;
;;; Parsing Text to Values
;;;

(define (cdr! in)
   (if (pair? (cdr in))
      (cdr in)
      (let* ((more ((cdr in))))
         more)))

(define (upto-newline in)
   (if (eq? (car in) 10)
      (cdr! in)
      (upto-newline (cdr! in))))

(define (cdr-whitespace! in)
   (cond
      ((eq? (car in) 32)
         (cdr-whitespace! (cdr! in)))
      ((eq? (car in) 10)
         (cdr-whitespace! (cdr! in)))
      ((eq? (car in) 13)
         (cdr-whitespace! (cdr! in)))
      ((eq? (car in) 59)
         (cdr-whitespace! (upto-newline in)))
      (else in)))

(define (leading-symbol-char? x)
   (cond
      ((< 59 x) (< x 127))
      ((< 41 x) (< x 48))
      ((< 34 x) (< x 40))
      ((eq? x 33) #t)
      (else #f)))

(define (digit? x)
   (if (< 47 x)
      (< x 58)
      #f))

(define (symbol-char? x)
   (if (leading-symbol-char? x)
      #t
      (digit? x)))

(define (get-symbol in rc)
   (if (symbol-char? (car in))
      (get-symbol (cdr! in) (cons (car in) rc))
      (cons
         (intern! (list->string (reverse rc)))
         in)))

(define (get-number in n)
   (if (digit? (car in))
      (get-number (cdr! in)
         (+ (* n 10)
            (- (car in) 48)))
      (cons n in)))

(define (get-list get-exp in out)
   (let* ((in (cdr-whitespace! in)))
      (cond
         ((eq? (car in) 41) ;; rp
            (cons (reverse out) (cdr! in)))
         ((eq? (car in) 46) ; .
            (let*
               ((in (cdr-whitespace! (cdr! in)))
                (res (get-exp in)))
               (if res
                  (let*
                     ((value (car res))
                      (inp (cdr-whitespace! (cdr res))))
                     (if (eq? (car inp) 41) ;; rp
                        (let* ((out (append (reverse out) value)))
                           (cons out (cdr! inp)))
                        #f))
                   #f)))
         (else
            (let* ((res (get-exp in)))
               (if res
                  (get-list get-exp (cdr res) (cons (car res) out))
               (let* ((x (display-chars (list 63 63 63 63 10))))
                  (get-exp (cdr! in)))))))))

(define (binary-tail! in n)
   (if (car in)
      (cons
         (+ (* (car in) 2) n)
         (cdr in))
      in))

(define (read-binary! in)
   (cond
      ((eq? (car in) 48)
         (binary-tail! (read-binary! (cdr! in)) 0))
      ((eq? (car in) 49)
         (binary-tail! (read-binary! (cdr! in)) 1))
      (else
         (cons 0 in))))

(define (get-sharp in)
   (cond
      ((eq? (car in) 116) ;; t
         (cons #t (cdr! in)))
      ((eq? (car in) 102) ;; f
         (cons #f (cdr! in)))
      ((eq? (car in) 98) ;; #b101010
         (read-binary! (cdr! in)))
      (else
         #f)))

(define (get-string in rcs)
   (if (eq? (car in) 34)
      (cons
         (list->string (reverse rcs))
         (cdr! in))
      (get-string (cdr! in)
         (cons (car in) rcs))))

;; in -> (exp . in')
(define (get-exp inp)
   (let* ((in (cdr-whitespace! inp)))
      (cond
         ((eq? (car in) 40)
            (get-list get-exp (cdr! in) '()))
         ((eq? (car in) 32)
            (get-exp (cdr! in)))
         ((eq? (car in) 35) ;; #
            (get-sharp (cdr! in)))
         ((eq? (car in) 34) ;; "
            (get-string (cdr! in) '()))
         ((eq? (car in) 39) ;; '
            (let* ((res (get-exp (cdr! in))))
               (if res
                  (cons (list 'quote (car res)) (cdr res))
                  #f)))
         ((eq? (car in) 45) ; -
            (let* ((in (cdr! in)))
               (if (digit? (car in))
                  (let* ((res (get-number (cdr! in) (- (car in) 48))))
                     (cons (negate (car res)) (cdr res)))
                  (cons '- in))))
         ((leading-symbol-char? (car in))
            (get-symbol (cdr! in) (list (car in))))
         ((digit? (car in))
            (get-number (cdr! in) (- (car in) 48)))
         (else
            (let* ((x (display-chars (list 63 63 63 10))))
               (get-exp (cdr! in)))))))


;;;
;;; Compiler
;;;

(define (code->function code)
   (set-pig (cons code '()) 0))

(define (mk-inst-unary opcode a)
   (+ opcode (* a 64)))

(define (mk-inst opcode a b)
   (mk-inst-unary opcode (+ a (* b 256))))

;; the ones that are primops can be omitted from definitions to save some space
(define op-apply           1)
(define op-close           2)
(define op-load-pos        3)
(define op-return          4)
(define op-load-env        5)
(define op-call            6)
(define op-cons            7)
(define op-car             8)
(define op-cdr             9)
(define op-load-immediate 10)
(define op-load-value     11)
(define op-equal          12)
(define op-if             13)
(define op-add            14)
(define op-sub            15)
(define op-set            16)
(define op-pig            17)
(define op-set-pig        18)
(define op-mul            19)
(define op-load-car       20)  ;; (op-load-car (val . _)) -> val
(define op-set-car        21)
(define op-set-cdr        22)
(define op-tail-call      23)
(define op-absorb         24)  ;; input byte
(define op-emit           25)  ;; output byte
(define op-div            26)
(define op-less           27)
(define op-gc             28)
(define op-swap-handler   29)
(define op-system         30)
(define op-commit         31)
(define op-resume         32)
(define op-bor            33)
(define op-band           34)
(define op-bxor           35)
(define op-bnot           36)
(define op-select         37) ;; push if cont
(define op-join           38) ;; pop if cont
(define op-arity          39) ;; arity check, fixed
(define op-halt           40) ;; arity check, fixed
(define op-narity         42) ;; arity check, n+ + reformat stack

(define (lookup-toplevel env sym)
   (getq env sym #f))

(define (stack-find-at s exp offset)
   (cond
      ((eq? s '()) #f)
      ((has? (car s) exp) offset)
      (else
         (stack-find-at (cdr s) exp (+ offset 1)))))

;; s has just symbols, update to lists later
(define (stack-find s exp)
   (stack-find-at s exp 0))

;; start depth 0
(define (env-find e exp depth)
      (if (eq? e '())
         #f
         (let* ((p (stack-find (car e) exp)))
            (if p
               (cons depth p)
               (env-find (cdr e) exp (+ depth 1))))))

;; warning: primop arities are not currently checked
(define *primops*
   (list
      (cons '+ op-add)
      (cons '* op-mul)
      (cons '/ op-div)
      (cons '- op-sub)
      (cons 'eq? op-equal)
      (cons 'emit op-emit)
      (cons 'absorb op-absorb)
      (cons '< op-less)
      (cons 'apply op-apply)
      (cons 'cons op-cons)
      (cons 'car op-car)
      (cons 'cdr op-cdr)
      (cons 'pig op-pig)
      (cons 'set-pig op-set-pig)
      (cons 'set-car! op-set-car)
      (cons 'set-cdr! op-set-cdr)
      (cons 'gc op-gc)
      (cons 'swap-handler! op-swap-handler)
      (cons 'system op-system)
      (cons '_commit op-commit)
      (cons '_resume op-resume)
      (cons 'bnot op-bnot)
      (cons 'band op-band)
      (cons 'bor op-bor)
      (cons 'bxor op-bxor)
      (cons 'halt op-halt)
      (cons 'nrefs 41)
      (cons 'error 43)
      ))

(define (primitive->inst exp)
   (getq *primops* exp #f))

(define primop?
   primitive->inst)

(define (car? x)
   (lambda (e) (and (pair? e) (eq? (car e) x))))

;; switch to a proper pattern match later
(define lambda? (car? 'lambda))
(define quote? (car? 'quote))
(define define? (car? 'define))
(define if? (car? 'if))
(define exec? (car? 'exec))

(define (primop-call? exp)
   (if (pair? exp)
      (primop? (car exp))
      #f))

(define (primitive-call rator rands s)
   (let* ((rands (map (lambda (x) (stack-find s x)) rands)))
      (cond
         ((null? rands)
            (mk-inst-unary (primitive->inst rator) 0))
         ((null? (cdr rands))
            (mk-inst-unary (primitive->inst rator) (car rands)))
         (else
            (mk-inst (primitive->inst rator) (car rands) (cadr rands))))))

(define (prim-call primop args)
   (cond
      ((null? args)
         (mk-inst-unary primop 0))
      ((null? (cdr args))
         (mk-inst-unary primop (car args)))
      (else
         (mk-inst primop (car args) (cadr args)))))

(define ok (list 101)) ;; just eq?

(define (first-unavailable s lst)
   (cond
      ((eq? lst '()) ok) ;; <- hackish
      ((stack-find s (car lst))
         (first-unavailable s (cdr lst)))
      (else (car lst))))

(define (offsets s es)
   (map
      (lambda (x)
         (stack-find s x))
      es))

(define (load-args env s e args compiler cont)
   (let* ((hard (first-unavailable s args)))
      (if (eq? hard ok)
         (cont s (offsets s args))
         (let* ((s-code (compiler hard env s e)))
            (if s-code
               (let* ((s-rest (load-args env (car s-code) e args compiler cont)))
                  (cons
                     (car s-rest)
                     (append (cdr s-code) (cdr s-rest))))
               #f)))))

(define (add-name s name-val)
   (if (has? (car s) (cdr name-val))
      (cons
         (append (car s) (list (car name-val)))
         (cdr s))
      (cons (car s) (add-name (cdr s) name-val))))

;; todo, does not shadow
(define (add-names s exps names)
   (fold add-name s (zip cons names exps)))

;; stack node -> symbol = binding, other value -> the value (quote sym) for symbols
;; metadata: is-pair, is-fixnum, ...

(define (inst-op n)
   (band n #b111111))

(define (inst-a  n)
   (band (/ n 64) #b11111111))

(define (inst-b n)
   (/ n 16384))

(define (unjoin lst)
   (if (eq? (car lst) op-join)
      '()
      (cons (car lst)
         (unjoin (cdr lst)))))

(define (optimize-tailcalls code)
   (if (null? code)
      code
      (let*
         ((op (inst-op (car code)))
          (a  (inst-a  (car code)))
          (b  (inst-b  (car code))))
         (cond
            ((eq? op op-call)
               (if (null? (cddr code))
                  (list (mk-inst op-tail-call a b)
                        (cadr code))
                  (cons (car code)
                     (cons (cadr code)
                        (optimize-tailcalls (cddr code))))))
            ((eq? op op-if)
               (cons (car code)
                  (cons (optimize-tailcalls (cadr code))
                        (optimize-tailcalls (cddr code)))))
            ((eq? op op-close)
               (cons (car code)
                  (cons (optimize-tailcalls (cadr code))
                        (optimize-tailcalls (cddr code)))))
            ((eq? op op-select)
               ;; optimize each branch + cont
               (let*
                  ((branches (cadr code))
                   (cont (cddr code)))
                  (if (null? cont)
                     (cons
                        (mk-inst op-if a b)
                        (cons
                           (optimize-tailcalls
                              (unjoin (car branches)))
                           (optimize-tailcalls
                              (unjoin (cdr branches)))))
                     (cons (car code)
                        (cons
                           (cons (optimize-tailcalls (car branches))
                                 (optimize-tailcalls (cdr branches)))
                           (optimize-tailcalls cont))))))
            ((has? (list op-load-value op-load-car op-call) op)
               (cons (car code)
                  (cons (cadr code)
                     (optimize-tailcalls (cddr code)))))
            (else
               (cons (car code)
                  (optimize-tailcalls (cdr code))))))))

(define (compiler exp env s e)
   (cond
      ((number? exp)
         (cons (cons (list exp) s) (list op-load-value exp)))
      ((quote? exp)
         (cons (cons (list exp) s) (list op-load-value (cadr exp))))
      ((eq? exp '())
         (cons (cons (list exp) s) (list op-load-value exp)))
      ((boolean? exp)
         (cons (cons (list exp) s) (list op-load-value exp)))
      ((string? exp)
         (cons (cons (list exp) s) (list op-load-value exp)))
      ((symbol? exp)
         (let* ((pos (stack-find s exp)))
            (if pos
               ;; already in stack, no need to load
               ;; can be optimized in various places
               (cons
                  (cons (list exp) s)
                  (list (mk-inst-unary op-load-pos pos)))
               (let* ((epos (env-find e exp 0))) ;; lexical binding in env?
                  (if epos
                     (cons
                        (cons (list exp) s)
                        (list (mk-inst op-load-env (car epos) (cdr epos))))
                     (let* ((node (lookup-toplevel env exp))) ;; toplevel binding?
                        (if node
                           (cons
                              (cons (list exp) s) ;; could also add the value
                              (list op-load-car node))
                           (error (list "ERROR: unbound " exp)))))))))
      ((if? exp)
         (let*
            ((sc (compiler (cadr exp) env s e))
             (sp-then (compiler (car (cddr exp)) env (car sc) e))
             (then (append (cdr sp-then) (list op-join)))
             (sp-else (compiler (cadr (cddr exp)) env (car sc) e))
             (else (append (cdr sp-else) (list op-join))))
            (cons
               (cons (list exp) (car sc))
               (append (cdr sc)
                  (list
                     (mk-inst op-select (stack-find (car sc) (cadr exp)) 0)
                     (cons then else))))))
      ((lambda? exp)
         (let*
            ((x-formals (cadr exp))
             (fixed? (list? x-formals))
             (formals (enlist x-formals))
             (body (car (cddr exp)))
             (sp-code
                (compiler body env
                   (map (lambda (x) (list x)) formals)
                   (cons s e))))
            (if sp-code
               (cons
                  (cons (list exp) s)
                  (list op-close
                     (cons
                        (if fixed?
                           (mk-inst-unary op-arity (length formals))
                           (mk-inst-unary op-narity (- (length formals) 1)))
                        (cdr sp-code))))
               #f)))
      ((list? exp)
         (cond
            ((primop-call? exp)
               (load-args env s e (cdr exp) compiler
                  (lambda (s args)
                     (cons
                        (cons (list exp) s)
                        (list (prim-call (primitive->inst (car exp)) args))))))
            ((lambda? (car exp))
               (load-args env s e (cdr exp) compiler
                  (lambda (s args)
                     (let*
                        ((op (car exp))
                         (res
                            (compiler (car (cddr op)) env
                              (add-names s (cdr exp) (cadr op))
                              e)))
                        (if res
                           (let* ((s (car res))
                                 (s (cons (cons exp (car s)) (cdr s))))
                              (cons s (cdr res)))
                           res)))))
            (else
               ;; is there something we can't refer from stack yet?
                  (load-args env s e exp compiler
                     (lambda (s args)
                        (cons
                           (cons (list exp) s)
                           (list
                              (mk-inst op-call (car args) (length (cdr args)))
                              (reverse (cdr args)))))))))
      (else
         #f)))

(define (compile exp env)
   (let* ((sc (compiler exp env '() '())))
      (if sc
         (let*
            ((code (optimize-tailcalls (cdr sc)))
             (code (if (null? code) (list op-return) code)))
            (code->function code))
         #f)))

(define (eval exp env)
   (let* ((res (compile exp env)))
      (if res
         (res)
         (begin
            (print (list "cannot compile " exp))
            #f))))





;;;
;;; Macro Expansion
;;;

(define (match-pat found exp pat)
   (cond
      ((not found) found)
      ((eq? pat '?)
         (cons exp found))
      ((pair? pat)
         (if (pair? exp)
            (match-pat
               (match-pat found (cdr exp) (cdr pat))
               (car exp) (car pat))
            #f))
      ((eq? exp pat)
         found)
      (else #f)))

(define (match pat exp)
   (match-pat '() exp pat))

(define macro-conversions
   (list


      ;;; lambda
      (cons '(lambda ? ? ? . ?)
         (lambda (vs e1 e2 en)
            (list 'lambda vs (cons 'begin (cons e1 (cons e2 en))))))

      ;;; let*
      (cons '(let* () . ?)
         (lambda (es) (cons 'begin es)))

      (cons '(let* ((? ?) . ?) . ?)
               (lambda (k v r es)
                  (list (list 'lambda (list k) (cons 'let* (cons r es))) v)))

      ;;; begin
      (cons '(begin ?)     (lambda (e) e))
      (cons '(begin)       (lambda () #f))
      (cons '(begin ? . ?) (lambda (e es)
                              (list (list 'lambda (list (gensym))
                                          (cons 'begin es))
                                    e)))
      ;;; define
      (cons '(define (? . ?) . ?) (lambda (k fs es)
         (list 'define k (list 'lambda fs (cons 'begin es)))))

      ;;; if
      (cons '(if ? ?) (lambda (test then) (list 'if test then #f)))
      (cons '(if (not ?) ? ?) (lambda (anti-test then else)
         (list 'if anti-test else then)))

      ;;; cond
      (cons '(cond) (lambda () #f))
      (cons '(cond (else . ?)) (lambda (es) (cons 'begin es)))
      (cons '(cond (? . ?) . ?)
         (lambda (test thens elses)
            (list 'if test (cons 'begin thens)
               (cons 'cond elses))))

      (cons '(if (not ?) ? ?) (lambda (anti-test then else)
         (list 'if anti-test else then)))
      ;;; and
      (cons '(and) (lambda () #t))
      (cons '(and ?) (lambda (x) x))
      (cons '(and ? . ?)
         (lambda (x xs)
            (let* ((s (gensym)))
               (list
                  (list 'lambda (list s) (list 'if s (cons 'and xs) #f))
                  x))))

      ;;; or
      (cons '(or) (lambda () #f))
      (cons '(or ?) (lambda (x) x))
      (cons '(or ? . ?)
         (lambda (x xs)
            (let* ((s (gensym)))
               (list
                  (list 'lambda (list s) (list 'if s s (cons 'or xs)))
                  x))))

))


;; -> (cont expanded) | exp
(define (expand-loop exp pats cont)
   (if (null? pats)
      exp
      (let* ((res (match (caar pats) exp)))
         (if res
            (let* ((resp (apply (cdar pats) res)))
               (cont resp))
            (expand-loop exp (cdr pats) cont)))))

(define (expand exp)
   (let* ((expanded (expand-loop exp macro-conversions expand)))
      (if (pair? expanded)
         (cond
            ((eq? (car expanded) 'quote)
               expanded)
            ((eq? (car expanded) 'lambda)
               (list 'lambda (cadr expanded) (expand (car (cddr expanded)))))
            (else
               (map expand expanded)))
         expanded)))



;;;
;;; System, GPIO, ADC, etc
;;;


;; special pins
(define pico-onboard-led  25)   ;; user led, green power led on pico
(define pico-vsys-pin     29)   ;; measure VSYS/3 voltage (ADC)
(define pico-vbus-present 24)   ;; digital, is there power at VBUS (micro usb)?
(define pico-powersave    23)

(define (gpio-init pin) (system 0 pin))
(define (gpio-output pin) (system 18 (list pin 1)))
(define (gpio-input pin)  (system 18 (list pin 0)))
(define (gpio-set pin val)  ;; 1 / 0
   (system 1 (list pin val)))

(define (init-onboard-led)
   (gpio-init pico-onboard-led) ;; init (check)
   (gpio-output pico-onboard-led))

(define (led on?)
   (gpio-set pico-onboard-led (if on? 1 0)))

(define (adc-select-channel n)
   (system 6 n))

(define (adc-measure)
   (system 7 0))

(define (adc-gpio-init pin)
   (system 17 pin))

;; pin 29 / channel 3 is connected to VBUS
(define (voltage)
   (pin-input pico-vsys-pin)
   (adc-gpio-init pico-vsys-pin)
   (adc-select-channel 3)
   (let* ((r (adc-measure)))
      ;; not calibrated yet
      (/ (* r (* 323 3)) 4096)))

(define (uptime)
   (system 14 0))

(define (adc-measure-channel n)
   (adc-select-channel n)
   (adc-measure))

(define (temperature)
   (adc-measure-channel 4))

(define (gpio-set-input pin)
   (system 2 pin))

(define (gpio-read-digital pin)
   (system 12 pin))

(define (sleep-us us)
   (system 5 us))

(define (sleep-ms ms)
   (system 15 ms))

(define (blink)
   (led #t) (sleep-ms 100)
   (led #f) (sleep-ms 100))

(define (blinks n)
   (if (not (eq? n 0))
      (begin
         (blink)
         (blinks (- n 1)))))

(define (awake)
   (blinks 2))

;; keep track of known pin states

(define *pins* ;; (input . output)
   (cons '() '()))

(define (pin-high? pin)
   (gpio-set-input pin)
   (eq? 1 (gpio-read-digital pin)))


;;;
;;; Demo Program
;;;

(define (test-program n digital-pins analog-channels)
   (let* ((v (voltage)))
      (print "Voltage *100: " v "V")
      (blinks (/ v 100)))
   (print "Round " n)
   (map
      (lambda (pin)
         (print " - " pin ": " (pin-high? pin)))
      digital-pins)
   (map
      (lambda (channel)
         (display " + analog channel ")
         (display channel)
         (display ": ")
         (print (adc-measure-channel channel)))
      analog-channels)
   (sleep-ms 1000)
   (test-program (+ n 1) digital-pins analog-channels))

;; print gpio pin statuses and values of adc channels in a loop
(define (demo)
   (print "TEST PROGRAM")
   (test-program 1
      '(0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22)
      '(0 1 2 3)))


;;;
;;; REPL
;;;

(define (prompt)
   (let* ((now (gc)))
      (display (/ (* (car now) 100) (cdr now)))
      (display "% free: ")))

(define (read-line-pico rcs)
   (let* ((next (absorb)))
      (cond
         ((has? '(13 10) next)
            (emit 10)
            (append (reverse rcs)
               (cons 10
                  (lambda ()
                     (read-line-pico '())))))
         ((eq? next 8) ;; backspace
            (if (pair? rcs)
               (begin
                  (emit 8)
                  (read-line-pico (cdr rcs)))
               (read-line-pico rcs)))
         (else
            (emit next) ;; echo / no echo
            (read-line-pico (cons next rcs))))))

(define (read-line-unix rcs)
   (cons (absorb) (lambda () (read-line-unix rcs))))

(define (read-line)
   (prompt)
   (if (eq? 1 (system 16 0))
      (read-line-unix '())
      (read-line-pico '())))

(define (env-node! env name)
   (or (getn env name #f)
      (let* ((new (list name #f)))
         (set-cdr! env (cons new (cdr env)))
         new)))

(define (maybe-show-metadata env sym)
   (let* ((val (lookup-toplevel env sym)))
      (if (and val (pair? (cdr val)))
         (print ";; source: " (cadr val)))))

(define (error-handler msg cont)
   (lambda args ;; currently (), will later have vm state
      (if (= (length args) 2)
         ;; ((S E C D) message)
         (print "Error: " (cadr args))
         (print "VM error:"))
      (cond
         ((eq? (car *settings*) 0)
            ;; print error message and resume
            (print msg)
            (cont))
         ((eq? (car *settings*) 1)
            ;; exit on errors
            (print msg)
            (halt 1))
         (else
            (print "?")
            (halt 2)))))

(define (repl in env)
   (prompt)
   (let*
      ((res (get-exp in))
       (exp-orig (car res))
       (exp (expand exp-orig)))
      (cond
         ((define? exp)
            (let*
               ((name (cadr exp))
                (node (env-node! env name))
                (value (eval (car (cddr exp)) env)))
               (print  ";; defined " name)
               (set-car! (cdr node) value)
               ;; save sources?
               (if (cadr *settings*)
                  (set-cdr! (cdr node) (list exp-orig)))
               ;; resume after last definition on errors
               (swap-handler!
                  (error-handler ";; backtracked to last definition"
                     (lambda ()
                        (repl (read-line) env))))
               (repl (cdr res) env)))
         ((exec? exp)
            ;; drop current compiler, env, etc
            (swap-handler! (lambda () (emit 42)))
            (eval (cadr exp) env))
         (else
            (let* ((outcome (eval exp env)))
               (if (symbol? exp)
                  (maybe-show-metadata env exp))
               (print outcome)
               (repl (cdr res) env))))))

(define (commit . entryp)
   (let* ((version (car (cddr *settings*))))
      (if (null? entryp)
         (begin
            (_commit #f version)
            (blinks 2))
         (_commit (car entryp) version))))

(define (resume)
   (_resume (car (cddr *settings*))))

(define new-toplevel-env
   (list
        (list '+ +)
        (list '* *)
        (list '/ /)
        (list '- -)
        (list '% %)
        (list 'emit emit)
        (list 'adc-measure adc-measure)
        (list 'adc-measure-channel adc-measure-channel)
        (list 'append append)
        (list 'boolean? boolean?)
        (list 'caar caar)
        (list 'cadr cadr)
        (list 'cdar cdar)
        (list 'cddr cddr)
        (list 'caaar caaar)
        (list 'caadr caadr)
        (list 'cadar cadar)
        (list 'caddr caddr)
        (list 'cdaar cdaar)
        (list 'cdadr cdadr)
        (list 'cddar cddar)
        (list 'cdddr cdddr)
        (list 'display display)
        (list 'fold fold)
        (list 'foldn foldn)
        (list 'for-each for-each)
        (list 'eval eval)
        (list 'equal? equal?)
        (list 'expand expand)
        (list 'function? function?)
        (list 'getq getq)
        (list 'gensym gensym)
        (list 'all all)
        (list 'iota iota)
        (list 'intern intern)
        (list 'led   led)
        (list 'length length)
        (list 'lfold lfold)
        (list 'list? list?)
        (list 'lref lref)
        (list '*symbols* *symbols*)
        (list 'get-exp get-exp)
        (list 'map map)
        (list 'match match)
        (list 'not not)
        (list 'null? null?)
        (list 'number? number?)
        (list 'pair? pair?)
        (list 'list list)
        (list '= =)
        (list '> >)
        (list 'foldr foldr)
        (list 'print print)
        (list 'reverse reverse)
        (list 'sleep-ms sleep-ms)
        (list 'sleep-us sleep-us)
        (list 'uptime uptime)
        (list 'symbol? symbol?)
        (list 'blink blink)
        (list 'null? null?)
        (list 'zero? zero?)
        (list 'render render)
        (list 'error error)
        (list 'voltage voltage)
        (list 'temperature temperature)
        (list 'pin-input pin-input)
        (list 'gpio-output gpio-output)
        (list 'gpio-input gpio-input)
        (list 'gpio-set gpio-set)
        (list 'adc-gpio-init adc-gpio-init)
        (list 'adc-select-channel adc-select-channel)
        (list 'commit commit)
        (list 'resume resume)
        (list '*version* *version*)
        (list '*settings* *settings*)
        (list 'demo demo)))

(define (heap-version)
   (car (cddr *settings*)))

(define (start-repl)
   (print *version* "/" (heap-version))
   (repl
     (read-line)
     new-toplevel-env))

;;;
;;; Symbol Table Cleanup
;;;

;; We don't want to spend precious memory on remembering all symbols that have
;; been seen during the development, so remove symbols that have a reference just
;; in the symbol table.

'FOO 'BAR
(print
   (cons "Dropped symbols (must include at least one of FOO or BAR):"
      (foldn
         (lambda (dropped node)
            (if (and (eq? 2 (nrefs (car node))) (pair? (cdr node)))
               (let* ((this (car node)))
                  (set-car! node (cadr node))
                  (set-cdr! node (cddr node))
                  (cons this dropped))
               dropped))
         '()
         *symbols*)))

;;;
;;; Entry
;;;

(resume-on-errors!)
(save-sources!)

;; unless pin 1 is high (e.g. by connecting it to pin 39), try to load stored heap image
;; otherwise the builtin one will be used
(define (maybe-resume-stored-heap)
   (if (pin-high? 0)
      (begin
         (print "Not loading stored heap, pin 1 is high")
         #f)
      (resume)))

(define (main x)
   (init-onboard-led)
   (awake)
   (or (maybe-resume-stored-heap)
      (begin
         (swap-handler!
            (error-handler ";; reset" start-repl))
         (print "starting repl")
         (start-repl))))

(commit main)

; (exec (main 97))
