#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <inttypes.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define DATA 1024

void myy_exit(int n) {
   exit(n);
}
uint16_t databuff[DATA]; // a buffer for raw data (ADC)

// change to suit system
char emit(int n) {
   char buff[1];
   buff[0] = n & 255;
   //usleep(10000);
   return (write(1, buff, 1) == 1);
}
char get_char() {
   unsigned char buf[1];
   if (read(0, buf, 1) == 1) {
      return buf[0];
   };
   exit(0);
}

void init() {}
void panic_blink() {
   exit(42);
}

char write_heap(char *start, int cells, int ver) {
   int pos = 0;
   int fd = open("heap.c", O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR);
   if (fd < 0) {
      printf("vm: failed to open heap.c\n");
      return 0;
   }
   dprintf(fd, "#define CELLS %d\n", cells);
   dprintf(fd, "int heap[] = {");
   while(pos < cells * 2) {
      int val = heap[pos];
      dprintf(fd, "%d%s", val, (pos == cells*2 - 1) ? "" : ", ");
      pos++;
   }
   dprintf(fd, "};\n");
   close(fd);
   return 1; // always succeed
}

char load_heap(char *start, int cells, int ver) {
   printf("cannot load stored heap in unix\n");
   return 0; // always fail
}

char sys(int op, int arg, int brg) {
   if (op == 16)
      return 1; // system id, 0 = pico
   // printf("GPIO %d %d\n", op, arg);
   return 0;
}

