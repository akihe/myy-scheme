#define pairto(a, b, target) {int *this; if ((int) fp == MNULL && gc(MNULL, MNULL, S, E, C, D, T, T2, P) == 0) goto panic; this = fp; fp = (int *) cdr(fp); this[0] = a; this[1] = b; target = (int) this; }
#define consto(a, b, target) pairto(a, b, target); target |= 4
#define push(x)   consto(x, S, S);
#define pairp(a)  ((a & 7) == 0)
#define consp(a)  ((a & 7) == 4)
#define functionp(a) (pairp(a) && consp(car(a)))

int *start, *fp, *end;

void mark(int this) {
   int foo;
   int parent = MNULL;
   process:
      if(immediatep(this)) goto backtrack;
      foo = car(this);
      if (markp(foo)) goto backtrack;
      car(this) = setmark(parent);
      parent = (this|2);
      this = foo;
      goto process;
   backtrack:
      if (parent == MNULL)
         return;
      if (parent & 2) {
         parent = parent^2;
         foo = cdr(parent);
         cdr(parent) = unmark(car(parent));
         car(parent) = setmark(this);
         this = foo;
         goto process;
      }
      foo = cdr(parent);
      cdr(parent) = this;
      this = parent;
      parent = foo;
      goto backtrack;
}

int sweep() {
   int *pos = end - 2;
   fp = (int *) MNULL;
   int nfree = 0;
   while(start < pos) {
      int val = *pos;
      if (markp(val)) {
         *pos = unmark(val);
      } else {
         pos[1] = (int) fp;
         fp = pos;
         nfree++;
      }
      pos -= 2;
   }
   return nfree;
}

int gc(int a, int b, int s, int e, int c, int d, int t, int t2, int p) {
   int n;
   mark(a);mark(b);mark(s);mark(e);mark(c);mark(d);
   mark(t);mark(t2);mark(p);
   return sweep();
}

int lreft(int lst, int pos) {
   while(pos--) {
      lst = cdr(lst);
   }
   return lst;
}

int lref(int lst, int pos) {
   while(pos--) {
      lst = cdr(lst);
   }
   return car(lst);
}

int llen(int ptr) {
   int n = 0;
   while(ptr != MNULL) {
      n++;
      ptr = cdr(ptr);
   }
   return n;
}

void rebase_heap(int n) {
   int p = 0;
   while(p < CELLS*2) {
      int v = heap[p];
      if (allocp(v))
         heap[p] = v + n;
      p++;
   }
}

int heap_entry() {
   int pos = 0;
   while (pos < CELLS*2) {
      if (heap[pos] == MENTRY)
         return heap[pos - 1];
      pos++;
   }
   return 1; // odd pointers are not possible
}

int run() {
   int S, E, C, D, P, T, T2;
   unsigned char arity;
   int pos = 0;
   int *hp = start;
   int val;
   boot_heap:
   rebase_heap((int) heap); // point pointers relative to where we are
   T = heap_entry();
   if (T == 1) {
      printf("No boot entry\n");
      goto panic;
   }
   S = car(T);
   E = car(cdr(T));
   C = car(cdr(cdr(T)));
   D = car(cdr(cdr(cdr(T))));
   P = car(cdr(cdr(cdr(cdr(T)))));
   T = T2 = MNULL;
   gc(MNULL, MNULL, S, E, C, D, T, T2, P);
   push(fixnum(100)); // <- return value from (commit)
   arity = 1;

   resume:
   while (C != MNULL) {
      int inst = fixval(car(C));
      C = cdr(C);
      switch(inst&63) {
         case 11: /* op-load-value */
            consto(car(C), S, S);
            C = cdr(C);
            break;
         case 12: /* op-equal */
            consto(((lref(S, aof(inst)) == lref(S, bof(inst))) ? MTRUE : MFALSE), S, S);
            break;
         case 13: { /* op-if */
            C = ((lref(S, aof(inst)) == MFALSE) ? cdr(C) : car(C));
            break; }
         case 3:
            consto(lref(S, aof(inst)), S, S);
            break;
         case 8:
            T = lref(S, aof(inst));
            if (!consp(T)) { printf("CAR NON PAIR\n"); goto panic; }
            consto(car(T), S, S);
            T = MNULL;
            break;
         case 9:
            T = lref(S, aof(inst));
            if (!consp(T)) { printf("CDR NON PAIR %u, mnull %d\n", T, MNULL); goto panic; }
            consto(cdr(T), S, S);
            T = MNULL;
            //printf("done\n");
            break;
         case 7:
            consto(lref(S, aof(inst)), lref(S, bof(inst)), T);
            consto(T, S, S);
            break;
         case 5:
            consto(lref(lref(E, aof(inst)), bof(inst)), S, S);
            break;
         case 4: { /* op-return */
            int st;
            secd_return:
            if ((int) D == MNULL) {
               return fixval(car(S));
            }
            T = car(S);
            st = car(D);
            D = cdr(D);
            S = car(st); st = cdr(st);
            E = car(st); st = cdr(st);
            C = car(st);
            consto(T, S, S);
            T = MNULL;
            break; }
         case 2: { /* close */
            consto(S, E, T);
            pairto(car(C), T, T);
            C = cdr(C);
            consto(T, S, S);
            T = MNULL;
            break; }
         case 1: /* op-apply */
            consto(C, MNULL, T);
            consto(E, T, T);
            consto(S, T, T);
            consto(T, D, D);
            T = lref(S, aof(inst)); /* operator */
            S = lref(S, bof(inst)); /* args */
            E = cdr(T);
            C = car(T);
            T = MNULL;
            arity = llen(S);
            break;
         case 6: { /* call */
            consto(cdr(C), MNULL, T); // <- can shave a cons
            consto(E, T, T);
            consto(S, T, T);
            consto(T, D, D);
          tail_call:
            T2 = car(C); // reverse offsets
            T = lref(S, aof(inst)); /* operator */
            if (!functionp(T)) {
               printf("calling a non-function %d\n", T);
               goto panic;
            }
            E = cdr(T);
            C = car(T);
            T = MNULL;
            while(T2 != MNULL) {
               consto(lref(S, fixval(car(T2))), T, T);
               T2 = cdr(T2);
            }
            S = T;
            arity = bof(inst);
            break; }
         case 23: /* op-tail-call */
            goto tail_call;
         case 14: { /* add */
            int a = fixval(lref(S, aof(inst)));
            int b = fixval(lref(S, bof(inst)));
            consto(fixnum(a + b), S, S);
            break; }
         case 15: { /* sub */
            int a = fixval(lref(S, aof(inst)));
            int b = fixval(lref(S, bof(inst)));
            consto(fixnum(a - b), S, S);
            break; }
         case 17: /* pig */
            consto(fixnum(lref(S, aof(inst)) & 7), S, S);
            break;
         case 18: /* set-pig */
            consto((lref(S, aof(inst)) & ~4) | (fixval(lref(S, bof(inst))) & 4), S, S);
            break;
         case 19: { /* mul */
            int a = fixval(lref(S, aof(inst)));
            int b = fixval(lref(S, bof(inst)));
            push(fixnum(a * b));
            break; }
         case 25: { /* emit */
            int a = fixval(lref(S, aof(inst)));
            push((emit(a) ? MTRUE : MFALSE));
            break; }
         case 26: { /* div */
            int a = fixval(lref(S, aof(inst)));
            int b = fixval(lref(S, bof(inst)));
            if (!b)
               goto panic;
            push(fixnum(a / b));
            break; }
         case 20: { /* load-car */
            push(car(car(C)));
            C = cdr(C);
            break; }
         case 21: { /* set-car */
            int a = lref(S, aof(inst));
            int b = lref(S, bof(inst));
            car(a) = b;
            push(a);
            break; }
         case 22: { /* set-cdr */
            int a = lref(S, aof(inst));
            int b = lref(S, bof(inst));
            cdr(a) = b;
            push(a);
            break; }
         case 24: {
            push(fixnum(get_char()));
            break;
         case 27: /* op-less */
            push((lref(S, aof(inst)) < lref(S, bof(inst))) ? MTRUE : MFALSE);
            break;
         }
         case 28: { /* gc */
            int n;
            consto(MNULL, MNULL, T);
            push(T);
            n =
            car(T) = fixnum(gc(MNULL, MNULL, S, E, C, D, T, T2, P));
            cdr(T) = fixnum(CELLS);
            break;
         }
         case 29: { /* swap-panic-handler -> prev */
            push(P);
            P = lref(S, 1 + aof(inst));
            break;
         }
         case 30: { /* system op a1 a2 */
            int op = fixval(lref(S,aof(inst)));
            int arg = lref(S, bof(inst));
            if (consp(arg) && consp(cdr(arg))) {
               push(fixnum(sys(op, fixval(car(arg)), fixval(car(cdr(arg))))));
            } else {
               push(fixnum(sys(op, fixval(lref(S,bof(inst))), 0)));
            }
            if (op == 9) {
               uint16_t *pos = (uint16_t *) databuff + DATA - 1;
               T = MNULL;
               while(pos >= databuff) {
                  consto(fixnum(*pos), T, T);
                  pos--;
               }
               car(S) = T;
            }
            break;
         }
        case 31: { /* commit <entry|#false> <version-fixnum> */
            char res;
            T = T2 = MNULL;
            int ver = fixval(lref(S, bof(inst)));
            if (lref(S, aof(inst)) == MFALSE) {
               // (commit #f) = commit whole state
               gc(MNULL, MNULL, S, E, C, D, T, T2, P);
               // cons up a list of (S E C D P)
               consto(P, MNULL, T);consto(D, T, T);consto(C, T, T);
               consto(E, T, T);consto(S, T, T);
               // make ((S E C D P) . ENTRYTAG) for loading
               pairto(T,MENTRY,T);
               rebase_heap((int) heap * -1);
               res = write_heap((char *) start, CELLS, ver);
               rebase_heap((int) heap);
               push(res ? MTRUE : MFALSE);
               T = MNULL;
            } else {
               // (commit <func>) = just the function
               gc(MNULL, MNULL, S, E, C, D, T, T2, P);
               // cons up a list of (() cdr(T) car(T) () #f)
               T = MNULL;
               consto(MFALSE, T, T); // (#f)
               consto(MNULL, T, T);      // (() #f)
               T2 = lref(S, aof(inst));
               consto(car(T2), T, T);
               consto(cdr(T2), T, T);
               consto(MNULL, T, T);   // (() cdr(op) car(op) () #f)
               pairto(T, MENTRY, T);
               rebase_heap((int) heap * -1);
               res = write_heap((char *) start, CELLS, ver);
               rebase_heap((int) heap);
               push(res ? MNULL : MFALSE); // null = partial commit
            }
            T = MNULL;
            T2 = MNULL;
            break; }
        case 32: { /* resume */
            int *pos = start;
            int found = 0;
            int ver = fixval(lref(S, aof(inst)));
            if (!load_heap((char *) start, CELLS, ver)) {
               push(MFALSE);
               break;
            }
            printf("heap loaded, going to boot\n");
            goto boot_heap;
            break; }
         case 33: { /* bor */
            int a = fixval(lref(S, aof(inst)));
            int b = fixval(lref(S, bof(inst)));
            push(fixnum(a | b));
            break; }
         case 34: { /* and */
            int a = fixval(lref(S, aof(inst)));
            int b = fixval(lref(S, bof(inst)));
            push(fixnum(a & b));
            break; }
         case 35: { /* xor */
            int a = fixval(lref(S, aof(inst)));
            int b = fixval(lref(S, bof(inst)));
            push(fixnum(a ^ b));
            break; }
         case 36: { /* bit-not */
            int a = fixval(lref(S, aof(inst)));
            push(fixnum(!a));
            break; }
         case 37: { /* select */
            consto(cdr(C), D, D);  // push if cont
            consto(S, D, D);       // stack at the time of if
            C = (lref(S, aof(inst)) == MFALSE) ? cdr(car(C)) : car(car(C));
            break; }
         case 38: { /* join */
            T = car(S);  // return value
            S = car(D);
            D = cdr(D);
            C = car(D);
            D = cdr(D);
            consto(T, S, S);
            break; }
         case 39: { /* arity */
            if (arity != aof(inst)) {
               printf("arity error, wanted %d but got %d\n", aof(inst), arity);
               goto panic;
            }
            break; }
         case 40: { /* halt */
            return fixval(lref(S, aof(inst)));
            break; }
         case 41: { /* nrefs */
            int n = 0;
            int val = lref(S, aof(inst));
            int *pos = heap;
            while(pos < end)
               n += (*pos++ == val);
            push(fixnum(n));
            break; }
         case 42: { /* narity */
            int n = aof(inst);
            if (arity < n) {
               printf("arity error, wanted at least %d but got %d\n", n, arity);
               goto panic;
            }
            if (n == 0) {
               consto(S, MNULL, S);
            } else {
               T = S;
               while(--n) {
                  T = cdr(T);
               }
               consto(cdr(T), MNULL, T2);
               cdr(T) = T2;
            }
            break; }
         case 43: { /* error <why> */
            consto(D, MNULL, T);
            consto(C, T, T);
            consto(E, T, T);
            consto(S, T, T);
            T2 = lref(S, aof(inst));
            consto(T2, MNULL, S);
            consto(T, S, S);
            arity = 2;
            goto controlled_panic;
            break; }
         default:
            printf("vm: what inst is %d\n", inst&63);
            goto panic;
      }
   }
   goto secd_return;
   panic:
      S = MNULL;
      C = MNULL;
      E = MNULL;
      D = MNULL;
      arity = 0;
   controlled_panic:
      if (P != MNULL) {
         gc(MNULL, MNULL, S, E, C, D, T, T2, P);
         D = MNULL;
         E = cdr(P);
         C = car(P);
         arity = 0; // later the info
         goto resume;
     } else {
        exit(42);
     }
}

int main(int nargs, char **args) {
   size_t n = CELLS + 1;
   int rval;
   init();
   start = (int *) heap;   // <- can now be dropped, use just heap
   end = start + 2*CELLS;
   return run();
}
