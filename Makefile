
## UNIX binary: make myy

default:
	make reboot   # rebuild the heap from myy.scm
	make test     # run tests against the new binary

myy: myy.c
	cc -O2 -m32 -o myy myy.c

myy.c: prelude.c heap.c unix.c runtime.c
	cat prelude.c heap.c unix.c runtime.c > myy.c

reboot: myy
	cat myy.scm | ./myy
	make myy

test: myy tests.sh tests.scm
	./tests.sh

## PICO version: make myy.uf2

myy_scheme.c: prelude.c pico.c heap.c runtime.c
	# you can run make reboot first to include changes from myy.scm
	cat prelude.c heap.c pico.c runtime.c > myy_scheme.c

myy.uf2: build myy_scheme.c
	cd build && make
	cp build/myy_scheme.uf2 myy.uf2

pico-install: myy.uf2
	./pico-install.sh myy.uf2

deps:
	sudo apt install cmake gcc-arm-none-eabi libnewlib-arm-none-eabi gcc-multilib

pico-sdk:
	git clone https://github.com/raspberrypi/pico-sdk.git
	cd pico-sdk && git submodule update --init

build: pico-sdk myy_scheme.c
	mkdir -p build
	cd build && cmake ..

clean:
	-rm -rf build myy.c myy_scheme.c
	git checkout heap.c

chat:
	minicom -b 115200 -o -D /dev/ttyACM1

mrproper: clean
	-rm -rf pico-sdk


.phony: deps clean mrproper pico-install chat default
