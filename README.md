# Myy Scheme

Myy is a small Scheme system designed to run the Raspberry Pico boards and
other microcontrollers using the RP2040 chip and its successors. It can also
run on Linux.

It is written to be used in various personal electronics projects. It might
also be of interest to someone looking for a minimal standalone lisp
implementation.


## Status

Myy can be used interactively on at least Raspberry Pico and Tiny 2040,
and on a Linux system. Language features are being developed and the first
device running Myy is being built.


## Building

On Linux:

```
$ git clone https://haltp.org/git/myy.git
$ cd myy
$ make
$ ./myy   # or $ rlwrap ./myy
```

For Pico:

```
$ git clone https://haltp.org/git/myy.git
$ cd myy
$ make myy.uf2
$ cp myy.uf2 /mnt   # or where you mounted your Pico
```

## Usage

When the Pico is powered up and Myy starts running, it blinks the led twice.
Then, if a program state is found from the flash, it will be loaded and two
more rapid blinks will be made. In case you end up with a corrupted memory
image in flash, you can bypass loading it at startup by setting GPIO0 high.

You can make a quick check that communications are working as expected by doing
`echo '(led #t)' > /dev/ttyACM0` and checking that the led turns on. If it doesn't,
then likely your Pico is at `/dev/ttyACM1` or some other port. Check from dmesg.

Once you've found your Pico, you can have a longer chat with Myy e.g. by doing:

```
$ minicom -b 115200 -o -D /dev/ttyACM1
```


# Language

## Core Language

```
(lambda (arg ...) body)  ;; make an anonymous function
(if test then else)      ;; evaluate else if test evaluates to #f, otherwise then
(quote exp)              ;; alternatively just 'exp. do nothing, just return exp.
```

## Primitives

Primitives, or primops, call functionality provided by the virtual machine. Some of
these can be called as if they were functions, and some are only added to bytecode
in order to implement the core language features.

```
(+ a b)             ;; fixnum sum
(* a b)             ;; fixnum product
(/ a b)             ;; fixnum division
(- a b)             ;; fixnum substraction (negative numbers not yet in place)
(< a b)             ;; fixnum comparison
(eq? a b)           ;; object equality
(emit <byte>)       ;; output a byte via terminal or usb serial
(absorb)            ;; read a byte via terminal or usb serial
(apply op args)     ;; apply function to argument list
(cons a b)          ;; construct a pair
(car p)             ;; get the first field of a pair
(cdr p)             ;; get the second field of a pair
(pig val)           ;; get the PIG bits of a value
(set-pig val bits)  ;; set the P   bit  of a value (others are ignored)
(set-car! p v)      ;; update the first field of a pair
(set-cdr! p v)      ;; update the second field of a pair
(gc)                ;; run gc and return (nfree . heapsize)
(swap-handler! fn)  ;; set error handler function
(system a b)        ;; system call
(_commit fn ver)    ;; store program state on flash (pico) or write heap.c (linux)
(_resume ver)       ;; load program state from flash if version matches(pico)
(bnot a)            ;; fixnum bitwise not
(band a b)          ;; fixnum bitwise and
(bor a b)           ;; fixnum bitwise or
(bxor a b)          ;; fixnum bitwise exclusive or
(halt n)            ;; stop execution
(nrefs a)           ;; count number of references to a in the heap
(error a)           ;; call the error handler with message a
```

## Macros

Macros are program transformations that take place before the program is
evaluated. The set of macros is currently hardcoded, because this was
convenient while bootstrapping Myy.

```
(begin A B ... N)
(let* ((key val) ...) exp ..)
(cond (test exp ...) [(else default)])
(and exp ...)
(or exp ...)
```

Additionally `lambda`-expressions with multiple body expressions are converted
to have begins in them internally, and `if`-expressions without one branch gets
`#f` as the return value.

## REPL Special Forms

Some expressions have special meaning when typed to the REPL.

### Definitions

```
(define name value)
(define (name arg ...) value) ;; same as (define name (lambda (arg ...) value))
```

### Stopping the REPL

The code for the interactive system takes about half of the available memory.
You can free this and run just a given function by calling `exec`. This is
mainly useful if you need more than about 100KB of memory, or if you are
updating the whole REPL and want to drop the earlier version from memory.


## Library Functions

### Digital Input and Output

`(gpio-init pin)`: Initialize a pin to be used for digital GPIO.

`(gpio-output pin)`: Setup pin (number) to be used for output.

`(gpio-input pin)`: Setup pin (number) to be used for input.

`(gpio-set pin 1/0)`: Set an output pin high or low.


### Analog Input

`(adc-gpio-init pin)`: Initialize a suitable pin to be used for ADC.

`(adc-select-channel n)`: Select ADC channel, typically 0-2, for measurement.

`(adc-measure)`: Measure the selected ADC channel. Returns 0-4096.


### Onboard Devices

`(voltage)`: Measure the VSYS voltage on Pico. Returns Voltage * 100 as fixnum.

`(temperature)`: Measure internal temperature from ADC channel 4. Not currently calibrated.

### Suspend / Resume

`(commit)`: Store the entire VM memory to flash to be resumed later.

`(resume)`: Load the program state from flash. This is done automatically at powerup.


## Virtual Machine

Myy uses a traditional SECD-style virtual machine. The name of the machine
is an acronym of Stack, Environment, Code and Dump, which are the registers
of the machine. Each register holds a Scheme value, so it has type
information associated to it.

All of the registers point to linked lists. The stack is initially a list of the
arguments of a function. The environment points to a list of lists of values.
The dump is another stack into which one can save the stack, environment and
code to be retrieved later. Function calls, when not handled as mere jumps,
push the current stack, environment and remaining code to dump before making
the call, and returns pop them back in place along with the return value.


## Memory

Memory is managed by a Deutch-Schorr-Waite -style garbage collector. It can mark
live objects and reclaim unused cells with just a few stack frames and one
bit of working space per descriptor.

Objects are represened in memory in what the code refers to as PIG-encoding,
which is an acronym for Pair, Immediate, Garbage. These denote the order and
function of three lowest bits of a 32-bit value, which are known to be zero
in pointers to even 32-bit word addresses. Pair is a single-bit type tag for
cons cells and fixnums. Immediate bit tells whether the value is a pointer or
holds some data in itself. Garbage bit is always zero, except during GC. An
important property of this encoding is that a pointer to an allocated non-cons
pair is just a regular pointer on the target platform.



## Bootstrapping

Rebuild the compiler using its previous version and run tests on Linux:

```
$ make
```
Rebuild the compiler on Pico:
 - send myy.scm using cat or zmodem ascii upload in minicom to Pico running Myy
 - call `(exec (start-repl))` on Pico
    - this stops the old REPL by tailcalling start of the new repl
 - possibly call `(commit #f)` to be able to load the new version later using `(resume)`

