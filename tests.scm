
(define results (cons 0 0))

(define (check value result)
   (set-cdr! results (+ 1 (cdr results)))
   (if (equal? value result)
      (begin
         (print (list "OK " value " = " result))
         (set-car! results (+ 1 (car results))))
      (begin
         (print (list "Computer says no: " value " != " result))
         (halt 1))))


;; math
(check (+ 1 2) (+ 2 1))
(check (+ 1 2) (+ 2 1))
(check (* 123 2) (+ 123 123))
(check (/ 12345 12) 1028)
(check (% 123456 123) 87)
(check (< 1 2) #t)
(check (< 1 1) #f)
(check  59 (band 123 63))
(check 127 (bor 123 12))
(check 127 (bor 123 12))
(check  90 (bxor 170 240))

;; pairs
(check 1 (car (cons 1 2)))
(check 2 (cdr (cons 1 2)))
(define a (let* ((a (cons 11 22))) (set-car! a 101) (set-cdr! a a) a))
(check #t (eq? a (cdr a)))
(check 101 (car a))

;; other primitives
(check #t (eq? 0 0))
(check #t (eq? #t #t))
(check #f (eq? #t #f))
(check #f (eq? (lambda (x) x) (lambda (x) x)))
(check #t (let* ((a (lambda (x) x))) (eq? a a)))

;; control
(check 11 (if #t 11 22))
(check 22 (if #f 11 22))
(check #t (or #f #f #t #f))
(check #f (or #f #f #f #f))
(check #t (and #t #t #t #t))
(check #f (and #t #t #f #t))

;; scope
(check 42 ((lambda (x) x) 42))
(check 42 ((lambda (x y) x) 42 10))
(check 42 ((lambda (x y) y) 10 42))

;; closures
(define (k a) (lambda (b) a))
(define (kp a) (lambda (b) (lambda (c) a)))

(check 111 ((k 111) 222))
(check 11 (((kp 11) 22) 33))

;; apply
(check 1 (apply (lambda (a b c) a) '(1 2 3)))
(check 3 (apply (lambda (a b c) c) '(1 2 3)))

;; evaluation
(check
   (eval '(cons 1 2) '())
   (cons 1 2))

;; gc -> (nfree . heapsize)
(check #t (let* ((res (gc))) (< (car res) (cdr res))))
(define a (gc))
(define x (iota 0 1 1000))
(define b (gc))

(check (eq? (cdr a) (cdr b)) #t)
(check #t (> (car a) (car b)))

(define x '(((a . b) . (c . d)) . ((e . f) . (g . h))))

(check 'a (caaar x))
(check 'b (cdaar x))
(check 'c (cadar x))
(check 'd (cddar x))
(check 'e (caadr x))
(check 'f (cdadr x))
(check 'g (caddr x))
(check 'h (cdddr x))

(print results)

(halt (if (eq? (car results) (cdr results)) 0 1))

